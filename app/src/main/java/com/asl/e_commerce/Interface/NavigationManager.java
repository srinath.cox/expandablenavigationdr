package com.asl.e_commerce.Interface;

public interface NavigationManager {
    void showFragment(String title);
}
