package com.asl.e_commerce.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.asl.e_commerce.R;
import com.asl.e_commerce.model.GetAllProductData;
import com.asl.e_commerce.model.SampleProduct;
import com.asl.e_commerce.utils.ItemStyle;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterProducts extends RecyclerView.Adapter<AdapterProducts.MyViewHolder> {

    private List<GetAllProductData> productDataList;
    private Context context;
    private ItemStyle itemStyle;
    private boolean isClick=true;


    public AdapterProducts(List<GetAllProductData> productDataList, Context context, ItemStyle itemStyle) {
        this.productDataList = productDataList;
        this.context = context;
        this.itemStyle = itemStyle;
    }

    @NonNull
    @Override
    public AdapterProducts.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = null;

        //--------------- setting item style List or grid-----------------//
        if (itemStyle == ItemStyle.GRIDE_VIEW)
            itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_single_gride,
                    viewGroup, false);
        else if (itemStyle == ItemStyle.LIST_VIEW)
            itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_single_list,
                    viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterProducts.MyViewHolder myViewHolder, int position) {


        //----------------------Set model data to view-----------------//
        myViewHolder.tvItemName.setText("" + productDataList.get(position).getName());
        myViewHolder.tvItemPrice.setText("Tk. " + productDataList.get(position).getRegularPrice());


        myViewHolder.ivAddWishList.setImageResource(R.drawable.ic_action_favorite_border);


        myViewHolder.ivAddWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isClick) {
                    myViewHolder.ivAddWishList.setImageResource(R.drawable.ic_action_favorite);
                    isClick = false;
                } else {
                    myViewHolder.ivAddWishList.setImageResource(R.drawable.ic_action_favorite_border);
                    isClick = true;
                }

            }
        });


        //----------------------Set product image to view----------------//
        Glide.with(context)
                .load(productDataList.get(position).getFeaturesImage())
                .override(100, 100)
                .into(myViewHolder.ivImage);

    }

    @Override
    public int getItemCount() {
        return productDataList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvItemName, tvItemPrice;
        ImageView ivImage, ivAddWishList;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tvTitle);
            tvItemPrice = itemView.findViewById(R.id.tvPrice);
            ivImage = itemView.findViewById(R.id.ivProductImage);
            ivAddWishList = itemView.findViewById(R.id.ivAddWishList);
        }
    }
}
