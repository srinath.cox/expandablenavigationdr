package com.asl.e_commerce.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllProductData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("pid")
    @Expose
    private String pid;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("brand_id")
    @Expose
    private Integer brandId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("regular_price")
    @Expose
    private String regularPrice;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("sale_price")
    @Expose
    private String salePrice;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("conditions")
    @Expose
    private String conditions;
    @SerializedName("features_image")
    @Expose
    private String featuresImage;
    @SerializedName("image_1")
    @Expose
    private String image1;
    @SerializedName("image_2")
    @Expose
    private String image2;
    @SerializedName("image_3")
    @Expose
    private String image3;
    @SerializedName("image_4")
    @Expose
    private String image4;
    @SerializedName("image_5")
    @Expose
    private String image5;
    @SerializedName("in_stock")
    @Expose
    private Integer inStock;
    @SerializedName("is_upcoming")
    @Expose
    private Integer isUpcoming;
    @SerializedName("category")
    @Expose
    private GetSingleProductCategory category;


    public GetAllProductData() {

    }


    public GetAllProductData(Integer id, String pid, Integer categoryId, Integer brandId, String name,
                             String regularPrice, String discountPercent, String salePrice,
                             String description, String conditions, String featuresImage,
                             String image1, String image2, String image3, String image4,
                             String image5, Integer inStock, Integer isUpcoming,
                             GetSingleProductCategory category) {
        this.id = id;
        this.pid = pid;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.name = name;
        this.regularPrice = regularPrice;
        this.discountPercent = discountPercent;
        this.salePrice = salePrice;
        this.description = description;
        this.conditions = conditions;
        this.featuresImage = featuresImage;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.image4 = image4;
        this.image5 = image5;
        this.inStock = inStock;
        this.isUpcoming = isUpcoming;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(String regularPrice) {
        this.regularPrice = regularPrice;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getFeaturesImage() {
        return featuresImage;
    }

    public void setFeaturesImage(String featuresImage) {
        this.featuresImage = featuresImage;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public Integer getInStock() {
        return inStock;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public Integer getIsUpcoming() {
        return isUpcoming;
    }

    public void setIsUpcoming(Integer isUpcoming) {
        this.isUpcoming = isUpcoming;
    }

    public GetSingleProductCategory getCategory() {
        return category;
    }

    public void setCategory(GetSingleProductCategory category) {
        this.category = category;
    }

}