package com.asl.e_commerce.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAllProductResponse {


    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<GetAllProductData> data = null;

    public GetAllProductResponse(Boolean success, List<GetAllProductData> data) {
        this.success = success;
        this.data = data;
    }

    public GetAllProductResponse() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<GetAllProductData> getData() {
        return data;
    }

    public void setData(List<GetAllProductData> data) {
        this.data = data;
    }

}