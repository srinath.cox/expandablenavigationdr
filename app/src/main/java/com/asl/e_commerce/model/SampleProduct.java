package com.asl.e_commerce.model;

public class SampleProduct {
    private String title, price, url;

    public SampleProduct(String title, String price, String url) {
        this.title = title;
        this.price = price;
        this.url = url;
    }

    public SampleProduct() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
