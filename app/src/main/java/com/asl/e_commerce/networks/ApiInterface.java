package com.asl.e_commerce.networks;

import com.asl.e_commerce.model.GetAllProductResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    //----------------------Get all product--------------------//
    @GET("products")
    Call<GetAllProductResponse> getAllProductData();

}
