package com.asl.e_commerce.utils;

import android.content.Context;
import android.content.Intent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.asl.e_commerce.view.LoginRegistration;
import com.asl.e_commerce.view.MainActivity;

public class ProgressBarAnimations extends Animation {

    private Context context;
    private ProgressBar progressBar;
    private TextView textView;
    private float from, to;

    public ProgressBarAnimations(Context context, ProgressBar progressBar, TextView textView,
                                 float from, float to) {

        this.context = context;
        this.progressBar = progressBar;
        this.textView = textView;
        this.from = from;
        this.to = to;

    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);

        float value = from + (to - from) * interpolatedTime;
        progressBar.setProgress((int) value);
        textView.setText((int) value + " %");

        if (value == to) {
            Intent intent = new Intent(context, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
