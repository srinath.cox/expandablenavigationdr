package com.asl.e_commerce.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.asl.e_commerce.R;

import org.ankit.gpslibrary.ADLocation;
import org.ankit.gpslibrary.MyTracker;

public class LoginRegistration extends AppCompatActivity implements MyTracker.ADLocationListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_registration);
        findLoc();
    }

    private void findLoc() {
        new MyTracker(getApplicationContext(), this).track();
    }

    @Override
    public void whereIAM(ADLocation loc) {
        System.out.println(loc);
        Log.d("MyLocations", "gps: " + loc);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
