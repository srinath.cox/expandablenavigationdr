package com.asl.e_commerce.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.asl.e_commerce.Interface.NavigationManager;
import com.asl.e_commerce.R;
import com.asl.e_commerce.adapter.AdapterProducts;
import com.asl.e_commerce.adapter.CustomExpandableListAdapter;
import com.asl.e_commerce.helper.FragmentNavigationManager;
import com.asl.e_commerce.model.GetAllProductData;
import com.asl.e_commerce.model.GetAllProductResponse;
import com.asl.e_commerce.model.SampleProduct;
import com.asl.e_commerce.networks.ApiClients;
import com.asl.e_commerce.networks.ApiInterface;
import com.asl.e_commerce.utils.Constraints;
import com.asl.e_commerce.utils.ItemStyle;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RecyclerView.OnItemTouchListener {

    private final String TAG = "ecommerse" + getClass().getSimpleName();


    private List<GetAllProductData> allProductDataList;
    private RecyclerView recyclerView;
    private AdapterProducts adapterProducts;
    private ItemStyle itemStyle;
    private Toolbar toolbar;


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;
    private String[] items;

    private ExpandableListView expandableListView;
    private ExpandableListAdapter adapter;
    private List<String> listTitle;
    private Map<String, List<String>> lstChild;
    private NavigationManager navigationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //for drawer
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        expandableListView = findViewById(R.id.navList);
        navigationManager = FragmentNavigationManager.getmInstance(this);

        initItems();

        View listHeaderView = getLayoutInflater().inflate(R.layout.nav_header, null, false);
        expandableListView.addHeaderView(listHeaderView);
        genData();

        addDrawerItems();
        setDrawers();

        if (savedInstanceState == null)
            selectFirstItemAsDefaults();


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("E-Commerce");


        init();

    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void selectFirstItemAsDefaults() {
        if (navigationManager != null) {
            String firstItems = listTitle.get(0);
            navigationManager.showFragment(firstItems);
            getSupportActionBar().setTitle(firstItems);
        }
    }

    private void init() {


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Shopping Cart");
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        toolbar.setSubtitle("Smarter shopping");


        //-----------------Getting user choice data ------------------------//
        SharedPreferences sharedPreferences = getSharedPreferences(Constraints.sharedPrefName, MODE_PRIVATE);

        String tmp = sharedPreferences.getString("itemViewKey", "grid");
        itemStyle = tmp.equalsIgnoreCase("grid") ? ItemStyle.GRIDE_VIEW : ItemStyle.LIST_VIEW;


        //---------- Defining recycler view and customized---------------//
        recyclerView = findViewById(R.id.rvProduct);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapterProducts);
        recyclerView.addOnItemTouchListener(this);

        Log.d(TAG, "item style: " + itemStyle);


        //------------Changing data view list or grid------------------//
        if (itemStyle == ItemStyle.LIST_VIEW)
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        else if (itemStyle == ItemStyle.GRIDE_VIEW)
            recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));


//        //-----------------Adding sample data to model-----------------//
//        allProductDataList = new ArrayList<>();
//        SampleProduct sampleProduct = new SampleProduct("Personalised Pens", "Tk. 23",
//                "https://cdnlive.williampenn.net/pub/media/catalog/product/cache/image/700x560/e9c3970ab036de70892d86c6d221abfe/W/P/WP01550-a.jpg"
//        );
//        allProductDataList.add(sampleProduct);
//        sampleProduct = new SampleProduct("Abigail Van Buren", " Tk. 63",
//                "https://cms.cloudinary.vpsvc.com//image/fetch/t_sitecore_images/f_auto,dpr_auto,w_700/https://s3-eu-west-1.amazonaws.com/sitecore-media-bucket/prod%2Fen-AU%2F%7BA2567039-297C-445F-8007-371806181746%7D"
//        );
//        allProductDataList.add(sampleProduct);
//        sampleProduct = new SampleProduct("Matadoor", "Tk. 20",
//                "https://images-eu.ssl-images-amazon.com/images/I/31qvLJTspeL._SY300_QL70_.jpg"
//        );
//        allProductDataList.add(sampleProduct);
//        sampleProduct = new SampleProduct("Econo DX", "Tk. 30",
//                "https://cdn.shopify.com/s/files/1/0275/6675/products/170517_Smile_More_Pens_WEB.jpg?v=1495222162"
//        );
//        allProductDataList.add(sampleProduct);
//
//        sampleProduct = new SampleProduct("Reveled", "Tk. 230",
//                "https://kali.training/wp-content/uploads/2017/06/kali-linux-revealed-book-mock-3.png"
//        );
//        allProductDataList.add(sampleProduct);
//
//        sampleProduct = new SampleProduct("Rich", "Tk. 430",
//                "https://www.iwillteachyoutoberich.com/wp-content/themes/iwt/assets/img/book/book_v2.JPG"
//        );
//        allProductDataList.add(sampleProduct);
//
//
//        //------------------ Adding all data to adapter class-----------------//
//        adapterProducts = new AdapterProducts(allProductDataList, getApplicationContext(), itemStyle);
//        recyclerView.setAdapter(adapterProducts);

        getAllProducts();
    }

    private void setDrawers() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("E-Commerce");
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);

                getSupportActionBar().setTitle(mActivityTitle);

                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);


    }

    private void addDrawerItems() {
        adapter = new CustomExpandableListAdapter(this, listTitle, lstChild);
        expandableListView.setAdapter(adapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                getSupportActionBar().setTitle(listTitle.get(groupPosition).toString());
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                getSupportActionBar().setTitle("E-Commerce");
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                //change fragment when click on item
                String selectedItems = ((List) (lstChild.get(listTitle.get(groupPosition))))
                        .get(childPosition).toString();


                getSupportActionBar().setTitle(selectedItems);

                if (items[0].equals(listTitle.get(groupPosition)))
                    navigationManager.showFragment(selectedItems);
                else
                    throw new IllegalArgumentException("Not Supported Fragment");
                mDrawerLayout.closeDrawer(Gravity.START);

                return false;
            }
        });
    }


    // Generate data for navigation
    private void genData() {
        List<String> title = Arrays.asList("Android", "iOS", "FLUTTER");
        List<String> childItem = Arrays.asList("Beginner", "Intermediate", "Advanced", "Professional");

        lstChild = new TreeMap<>();
        lstChild.put(title.get(0), childItem);
        lstChild.put(title.get(1), childItem);
        lstChild.put(title.get(2), childItem);

        listTitle = new ArrayList<>(lstChild.keySet());

    }


    // init item for navigation drawer
    private void initItems() {
        items = new String[]{"Android", "IOS", "FLUTTER"};
    }


    //--------------------Reload data view model when user choice------------//
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "item change: " + Constraints.RELOAD_PATH);

        // ----------------------Getting user choice value------------//
        SharedPreferences sharedPreferences = getSharedPreferences(Constraints.sharedPrefName, MODE_PRIVATE);
        String tmp = sharedPreferences.getString("itemViewKey", "grid");
        itemStyle = tmp.equalsIgnoreCase("grid") ? ItemStyle.GRIDE_VIEW : ItemStyle.LIST_VIEW;

        //--------------------Checking user choice and changing data view------------//
        if (Constraints.RELOAD_PATH) {
            Constraints.RELOAD_PATH = false;
            Log.d(TAG, "style check: " + itemStyle);

            if (itemStyle == ItemStyle.LIST_VIEW)
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            else if (itemStyle == ItemStyle.GRIDE_VIEW)
                recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));


            adapterProducts = new AdapterProducts(allProductDataList, getApplicationContext(), itemStyle);
            adapterProducts.notifyDataSetChanged();
            recyclerView.setAdapter(adapterProducts);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item))
            return true;


        switch (item.getItemId()) {
            case R.id.menu_notifications:

                break;
            case R.id.menu_Settings:
                Intent intent = new Intent(new Intent(MainActivity.this, SettingActivity.class));
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getAllProducts() {
        ApiInterface apiInterface = ApiClients.getClinet().create(ApiInterface.class);

        Call<GetAllProductResponse> call = apiInterface.getAllProductData();

        call.enqueue(new Callback<GetAllProductResponse>() {
            @Override
            public void onResponse(Call<GetAllProductResponse> call, Response<GetAllProductResponse> response) {
                try {

                    if (response.body().getSuccess()) {
                        Log.d(TAG, "data size: " + response.body().getData().size());

                        allProductDataList = response.body().getData();
                        adapterProducts = new AdapterProducts(allProductDataList, getApplicationContext(), itemStyle);
                        recyclerView.setAdapter(adapterProducts);

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            Log.d(TAG, "data size: " + response.body().getData().
                                    get(i).getName());
                        }

                    } else {
                        Log.d(TAG, "data size: " + response.body().getData().size());

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetAllProductResponse> call, Throwable t) {
                Log.d(TAG, "data size: " + t.toString());
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
