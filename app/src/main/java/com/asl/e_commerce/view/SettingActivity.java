package com.asl.e_commerce.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.asl.e_commerce.R;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_holder, new SettingsFragment(), "S")
                .commit();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
