package com.asl.e_commerce.view;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.asl.e_commerce.R;
import com.asl.e_commerce.utils.ProgressBarAnimations;

import in.codeshuffle.typewriterview.TypeWriterView;

public class SplashScreen extends AppCompatActivity {

    private ProgressBar progressBar;
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        progressBar = findViewById(R.id.pbProgress);
        textView = findViewById(R.id.tvProgress);

        progressBar.setProgressTintList(ColorStateList.valueOf(Color.BLUE));

        //Create Object and refer to layout view
        TypeWriterView typeWriterView = (TypeWriterView) findViewById(R.id.typeWriterView);

        //Setting each character animation delay
        typeWriterView.setDelay(4000);

        //Setting music effect On/Off
        typeWriterView.setWithMusic(false);

        //Animating Text
        typeWriterView.animateText("Developed By Alchemy Software Limited");

        progressBar.setMax(100);
        progressBar.setScaleY(3f);

        setAnimation();

    }

    private void setAnimation() {
        ProgressBarAnimations animations = new ProgressBarAnimations(this, progressBar, textView
                , 0f, 100f);
        animations.setDuration(5000);
        progressBar.setAnimation(animations);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }
}